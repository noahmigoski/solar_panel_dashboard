# Solar_Panel_Dashboard

A web dashboard to display data from a solar panel. Data is collected by an [Arduino](https://www.adafruit.com/product/5303) and sent over the network to a Raspberry Pi which runs the web server for the dashboard.
