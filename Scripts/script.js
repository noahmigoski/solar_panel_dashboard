// Temperature Chart

$.getJSON( "data.json", function(data) {
        temps = [];
        $.each(data.Temperatures, function( key, val ) {
        temps.push(val);
        });

console.log(temps)
document.getElementById("Current Temperature").innerHTML = "Current Temperature: " + (temps[temps.length - 1].y) ;

var parsed_data = temps.map(function(temps) {
      return {x: new Date(temps["x"]), y: temps["y"]};
 });

console.log(parsed_data)

new Chart("Ambient Temperature", {
  type: "scatter",
    data: {
      datasets: [{
        label: "Temperature vs. Time",
        pointRadius: 4,
        pointBackgroundColor: "rgba(255,0,0,1)",
        data: parsed_data
      }]
  },
  options: {
    legend: {display: true},
    scales: {
            x: {
               type: 'time',
                time: {
                        displayFormats: {
                        unit: 'day',
                        day: 'h:mm a'
                        }}
                },
            y: {
                    ticks: {min:0, max:25},
                    scaleLabel: {display: true,
                    labelString: "Temperature"}
      },
    }
}
});


})



// Humidity Chart


$.getJSON( "data.json", function(data) {
        humidities = [];
        $.each(data.Humidities, function( key, val ) {
        humidities.push(val);
        });

console.log(humidities)
document.getElementById("Current Humidity").innerHTML = "Current Humidity: " + (humidities[humidities.length - 1].y) ;

var parsed_data = humidities.map(function(humidities) {
      return {x: new Date(humidities["x"]), y: humidities["y"]};
 });

console.log(parsed_data)

new Chart("Ambient Humidity", {
  type: "scatter",
    data: {
      datasets: [{
        label: "Humidity vs. Time",
        pointRadius: 4,
        pointBackgroundColor: "rgba(0,0,255,1)",
        data: parsed_data
      }]
  },
  options: {
    legend: {display: true},
    scales: {
            x: {
               type: 'time',
                time: {
                        displayFormats: {
                        unit: 'day',
                        day: 'h:mm a'
                        }}
                },
            y: {
                    ticks: {min:0, max:25},
                    scaleLabel: {display: true,
                    labelString: "Humidity"}
      },
    }
}
});


})



// Pressure Chart


$.getJSON( "data.json", function(data) {
        pressures = [];
        $.each(data.Pressures, function( key, val ) {
        pressures.push(val);
        });

console.log(pressures)
document.getElementById("Current Pressure").innerHTML = "Current Pressure: " + (pressures[pressures.length - 1].y) ;

var parsed_data = pressures.map(function(pressures) {
      return {x: new Date(pressures["x"]), y: pressures["y"]};
 });

console.log(parsed_data)

new Chart("Ambient Pressure", {
  type: "scatter",
    data: {
      datasets: [{
        label: "Pressure vs. Time",
        pointRadius: 4,
        pointBackgroundColor: "rgba(0,255,0,1)",
        data: parsed_data
      }]
  },
  options: {
    legend: {display: true},
    scales: {
            x: {
               type: 'time',
                time: {
                        displayFormats: {
                        unit: 'day',
                        day: 'h:mm a'
                        }}
                },
            y: {
                    ticks: {min:0, max:25},
                    scaleLabel: {display: true,
                    labelString: "Pressure"}
      },
    }
}
});


})





