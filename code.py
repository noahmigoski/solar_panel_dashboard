import time
import board
import ipaddress
import ssl
import wifi
import socketpool
import adafruit_requests
# display stuff
import displayio
import terminalio
import adafruit_displayio_sh1107
from adafruit_display_text import label
import busio
import adafruit_ina219
import digitalio
from analogio import AnalogIn

#Thermo resistor

analog_in = AnalogIn(board.A0)

def get_voltage(pin):
    return (pin.value * 3.3) / 65536 - 1.63

def resistance(A0_voltage):
    return 9000 * (3.3/A0_voltage - 1) / 5

def res2temp(resistance):
    x = resistance / 1000
    return 0.4619434197336342 * x**2 -11.861245706867093 *x + 96.26415439437672

# Buttons
button_A = digitalio.DigitalInOut(board.D9)
button_A.direction = digitalio.Direction.INPUT
button_A.pull = digitalio.Pull.UP
button_B = digitalio.DigitalInOut(board.D6)
button_B.direction = digitalio.Direction.INPUT
button_B.pull = digitalio.Pull.UP
button_C = digitalio.DigitalInOut(board.D5)
button_C.direction = digitalio.Direction.INPUT
button_C.pull = digitalio.Pull.UP

relay = digitalio.DigitalInOut(board.D10)
relay.direction = digitalio.Direction.OUTPUT

displayio.release_displays()

from adafruit_bme280 import basic as adafruit_bme280

# Create sensor objects, using the board's default I2C bus.
i2c = board.I2C()
bme280 = adafruit_bme280.Adafruit_BME280_I2C(i2c)
ina219 = adafruit_ina219.INA219(i2c, 0x40)
ina219_2 = adafruit_ina219.INA219(i2c, 0x41)


bme280.sea_level_pressure = 1013.25

#print("Config register:")
#print("  bus_voltage_range:    0x%1X" % ina219.bus_voltage_range)
#print("  gain:                 0x%1X" % ina219.gain)
#print("  bus_adc_resolution:   0x%1X" % ina219.bus_adc_resolution)
#print("  shunt_adc_resolution: 0x%1X" % ina219.shunt_adc_resolution)
#print("  mode:                 0x%1X" % ina219.mode)
#print("")

# Wifi Stuff

TIMEOUT = None
HOST = "migoski.net"
PORT = 5000
pool = socketpool.SocketPool(wifi.radio)


# Get wifi details and more from a secrets.py file
try:
    from secrets import secrets
except ImportError:
    print("WiFi secrets are kept in secrets.py, please add them there!")
    raise


print("CONNECTING...")

print("My MAC addr:", [hex(i) for i in wifi.radio.mac_address])

print("Available WiFi networks:")
for network in wifi.radio.start_scanning_networks():
    print("\t%s\t\tRSSI: %d\tChannel: %d" % (str(network.ssid, "utf-8"),
            network.rssi, network.channel))
wifi.radio.stop_scanning_networks()

print("Found Wifi Network: %s"%secrets["ssid"])
try:
    wifi.radio.connect(secrets["ssid"], secrets["password"])
except:
    print("failed to connect to wifi")
else:
    print("Connected to %s!"%secrets["ssid"])
    print("My IP address is", wifi.radio.ipv4_address)


print("Creating Socket")
def send_data(data):
    with pool.socket(pool.AF_INET, pool.SOCK_STREAM) as s:
        s.settimeout(TIMEOUT)
        print("Connecting")
        s.connect((HOST, PORT))
        print("Sending Data")
        sent = s.send(str(data))
        print("done")
#    print("Receiving")
#    buff = bytearray(128)
#    numbytes = s.recv_into(buff)
#print(repr(buff))


# Check if display is connected
try:
    # Use for I2C
    i2c = board.I2C()
    display_bus = displayio.I2CDisplay(i2c, device_address=0x3C)

    # SH1107 is vertically oriented 64x128
    WIDTH = 128
    HEIGHT = 64
    BORDER = 2

    display = adafruit_displayio_sh1107.SH1107(display_bus, width=WIDTH, height=HEIGHT)

    # Make the display context
    splash = displayio.Group()
    display.show(splash)


    color_bitmap = displayio.Bitmap(WIDTH, HEIGHT, 1)
    color_palette = displayio.Palette(1)
    color_palette[0] = 0xFFFFFF  # White

    text1 = "Starting up"
    text_area_1 = label.Label(terminalio.FONT, text=text1, color=0xFFFFFF, x=8, y=8)
    text_area_2 = label.Label(terminalio.FONT, text=text1, color=0xFFFFFF, x=8, y=20)
    text_area_3 = label.Label(terminalio.FONT, text=text1, color=0xFFFFFF, x=8, y=32)
    text_area_4 = label.Label(terminalio.FONT, text=text1, color=0xFFFFFF, x=8, y=44)
    splash.append(text_area_1)
    splash.append(text_area_2)
    splash.append(text_area_3)
    splash.append(text_area_4)

except:
    display_enabled = False
else:
    display_enabled = True

finally:
    i = 0
    j = 0
    display_on = False
    while True:
        controller_temp = res2temp(resistance(get_voltage(analog_in)))
        current1 = ina219.current
        current2 = ina219_2.current
        bus_voltage1 = ina219.bus_voltage
        if i == 0:
            #print("\nTemperature: {:.1f} C".format(bme280.temperature))
            #print("Humidity: {:.1f} %".format(bme280.relative_humidity))
            #print("Pressure: {:.1f} hPa".format(bme280.pressure))
            #print("Altitude: {:.2f} meters".format(bme280.altitude))
            temp = bme280.temperature
            humidity = bme280.relative_humidity
            pressure = bme280.pressure
            altitude = bme280.altitude
            shunt_voltage1 = ina219.shunt_voltage
            bus_voltage2 = ina219_2.bus_voltage
            shunt_voltage2 = ina219_2.shunt_voltage
            send_data([temp, humidity, pressure, altitude, bus_voltage1, shunt_voltage1, current1, bus_voltage2, shunt_voltage2, current2, controller_temp])
            i = 120 * 5 # enter as the desired reset time in seconds times 5
        if not button_A.value:
            j=15
            text_area_1.text = "Temperature: " + str(temp)
            text_area_2.text = "Humidity: " + str(humidity)
            text_area_3.text = "Pressure: " + str(pressure)
            text_area_4.text = "Altitude: " + str(altitude)
        elif not button_B.value:
            j=15
            text_area_1.text = "Bus Voltage: " + str(bus_voltage1)
            text_area_2.text = "Shunt Voltage: " + str(shunt_voltage1)
            text_area_3.text = "Current: " + str((current1 + current2)/1000 )
            text_area_4.text = "Con. Temp: " + str(controller_temp)
        else:
            if j==0:
                text_area_1.text = ""
                text_area_2.text = ""
                text_area_3.text = ""
                text_area_4.text = ""
            elif j>0:
                j -= 1
        if not button_C.value:
            relay.value = not relay.value
        if controller_temp > 40 or (current1 + current2) > 6000:
            relay.value = False
        i -= 1
        time.sleep(.2)
