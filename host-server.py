#!/usr/bin/env python3
import socket
from datetime import datetime
import pytz
import json, time, random

# function to add data to JSON file
def write_json(new_data, field, filename='data.json'):
    with open(filename,'r+') as file:
          # First we load existing data into a dict.
        file_data = json.load(file)
        # Join new_data with file_data inside voltages
        file_data[field].append(new_data)
        # Sets file's current position at offset.
        file.seek(0)
        # convert back to json.
        json.dump(file_data, file, indent = 4)


TIMEOUT = None
HOST = "migoski.net"
PORT = 5000

print("Create Socket")

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.settimeout(TIMEOUT)
s.bind((HOST, PORT))
s.listen()

print("Accepting connections")

def convert_to_list(string):
    return string[1:-1].split(",")
print(convert_to_list("[1,2,3]"))

def save_data(new_data):
    new_data_pt_temp = {"x" : datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "y": new_data[0]}
    new_data_pt_humidity = {"x" : datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "y": new_data[1]}
    new_data_pt_pressure = {"x" : datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "y": new_data[2]}
    new_data_pt_altitude = {"x" : datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "y": new_data[3]}
    new_data_pt_bus_voltage1 = {"x" : datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "y": new_data[4]}
    new_data_pt_shunt_voltage1 = {"x" : datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "y": new_data[5]}
    new_data_pt_current1 = {"x" : datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "y": new_data[6]}
    print("recieved temp data: " + str(new_data_pt_temp))
    print("recieved humidity data: " + str(new_data_pt_humidity))
    print("recieved pressure data: " + str(new_data_pt_pressure))
    print("recieved altitude data: " + str(new_data_pt_altitude))
    print("recieved bus voltage data: " + str(new_data_pt_bus_voltage1))
    print("recieved shunt voltage data: " + str(new_data_pt_shunt_voltage1))
    print("recieved current data: " + str(new_data_pt_current1))
    write_json(new_data_pt_temp, "Temperatures")
    write_json(new_data_pt_humidity, "Humidities")
    write_json(new_data_pt_pressure, "Pressures")
    write_json(new_data_pt_altitude, "Altitudes")
    write_json(new_data_pt_bus_voltage1, "bus_voltage1")
    write_json(new_data_pt_shunt_voltage1, "shunt_voltage1")
    write_json(new_data_pt_current1, "current1")

# Listen for new data
while True:
    # my_datetime_est = my_datetime.astimezone(pytz.timezone('US/Eastern')).strftime('%Y-%m-%d %H:%M:%S')
    data = convert_to_list(s.accept()[0].recv(128).decode())
    if len(data) > 1:
        save_data(data)
    else:
        print("garbled message: ", data)
    
